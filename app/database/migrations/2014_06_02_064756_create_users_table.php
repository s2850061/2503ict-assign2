<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
  
  //Creates users table and columns
	public function up()
	{
		Schema::create('users', function($table) {
                   $table->increments('id');
                   $table->string('email')->unique();
                   $table->string('password')->index();
                   $table->string('name');
                   $table->enum('category', array('jobseeker', 'employer'));
                   $table->char('phone', 15);
                   $table->text('remember_token')->nullable();
                   $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  
  //Creates drop function for users table
	public function down()
	{
		Schema::drop('users');
	}

}
