<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
  
  //Creates applications table and columns
	public function up()
	{
	  Schema::create('applications', function($table) {
                   $table->increments('id');
                   $table->integer('user_id');
                   $table->integer('job_id');
                   $table->string('applicant');
                   $table->text('letter');
                   $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  
  //Creates drop function for applications
	public function down()
	{
		Schema::drop('applications');
	}

}
