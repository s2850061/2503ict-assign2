<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
  
  //Creates tobs table and columns
	public function up()
	{
		Schema::create('jobs', function($table) {
                   $table->increments('id');
                   $table->integer('user_id');
                   $table->string('title')->index();
                   $table->text('description')->index();
                   $table->string('location')->index();
                   $table->float('salary')->index();
                   $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  
  //Creates drop function for users
	public function down()
	{
		Schema::drop('jobs');
	}

}