<?php
class UsersTableSeeder extends Seeder {
  public function run()
    { $user=User::create(array('email'=>'oreo@gmail.com',
                               'password'=>Hash::make('123456'), 
                               'name'=>'Oreo', 
                               'category'=>'employer',
                               'phone'=>123456));
     
      $user=User::create(array('email'=>'timtam@gmail.com', 
                               'password'=>Hash::make('123456'),
                               'name'=>'Tim Tam',
                               'category'=>'jobseeker',
                               'phone'=>123456));
     
     $user=User::create(array('email'=>'mintslice@gmail.com',
                               'password'=>Hash::make('123456'), 
                               'name'=>'Mint Slice', 
                               'category'=>'employer',
                               'phone'=>123456));
     
      $user=User::create(array('email'=>'redbull@gmail.com', 
                               'password'=>Hash::make('123456'),
                               'name'=>'Red Bull',
                               'category'=>'jobseeker',
                               'phone'=>123456));
     
  }
}
