<?php
class JobsTableSeeder extends Seeder {
  public function run()
    { $job=Job::create(array('title'=>'Lecturer', 
                             'user_id'=>1,
                             'description'=>'Marketing Lecturer needed, Part-time hours, 25-30+ per week', 
                             'location'=>'Southport, QLD', 
                             'salary'=>75000));
     
      $job=Job::create(array('title'=>'Receptionist', 
                             'user_id'=>2,
                             'description'=>'Receptionist needed for Student Services, Part-time hours, 20-30 per week',
                             'location'=>'Southport, QLD',
                             'salary'=>42000));
     
     $job=Job::create(array('title'=>'Night Fill Assistant',
                            'user_id'=>3,
                            'description'=>'Night Fill Assistant needed, Full-time hours, 30+ per week', 
                            'location'=>'Ashmore, QLD',                                     
                            'salary'=>30000)); 
     
     $job=Job::create(array('title'=>'Checkout Assistant',
                            'user_id'=>4,
                            'description'=>'Checkout assistant needed, Part-time hours, 15-20 per week', 
                            'location'=>'Crestwood, QLD', 
                            'salary'=>10000));
     
  }
}