<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

    $this->call('JobsTableSeeder');
    $this->call('UsersTableSeeder');
    $this->call('ApplicationsTableSeeder');
		// $this->call('UserTableSeeder');
	}

}
