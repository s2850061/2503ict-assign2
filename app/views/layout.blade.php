<html>
  <head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    {{ HTML::style('css/style.css') }}
  </head>
  <body>
    @if (Auth::check())
    <div id="nav" class="container">
        <ul class="nav nav-tabs pull-right">
          <li class="active"><a href="#">Welcome {{ Auth::user()->name }}</a> </li>
          <li>{{link_to_route('user.logout', "Sign Out") }}</li>
        </ul>
    <h3>CareerSearch</h3>
    </div>
    
    @else
    <div id="nav" class="container">
        <ul class="nav nav-tabs pull-right">
          <li>{{ link_to_route('user.create', 'Create an account') }}</li>
          <li>{{ link_to_route('user.index', 'Log In') }}</li>
        </ul>
        <h3>CareerSearch</h3>
    </div>
    @endif
    
    
    <div id="main" class="container">
      
      <div id="column1" class="col-sm-3">
        @yield('column1')     
      </div> 

      <div id="column2" class="col-sm-9">
        @yield('column2') 
      </div>
    </div>
    <footer>
    Michael O'Hara-close S2850061 <a href='../public/docs/doc.html'>Documentation </a>
    </footer>
  </body>
</html>