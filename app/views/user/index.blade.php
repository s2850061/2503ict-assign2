@extends('layout')
@section('title')
Log In
@stop
@section('column2')
      {{ Form::open(array('route' => 'user.login')) }}
            {{ Form::label('email', 'Email Address:') }}
            {{ Form::text('email', null, array('class'=>'form-control')) }}
            {{ $errors->first('email') }}
        
            {{ Form::label('password', 'Password: ') }}
            {{ Form::password('password', array('class'=>'form-control')) }}
            {{ $errors->first('password') }}
            <br/>
            {{ Form::submit('Sign in', array('class'=>'btn btn-default')) }}

       {{ Form::close() }}
      
@overwrite