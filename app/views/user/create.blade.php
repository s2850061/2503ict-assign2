@extends('layout')
@section('title')
Create Account
@stop
@section('column2')
        <h1>Create an account</h1>
        <br/>
        {{ Form::open(array('action'=> 'UserController@store', 'files' => true)) }}
              {{ Form::label('email', 'Email Address: ') }}
              {{ Form::text('email', null, array('class'=>'form-control'))  }}
              {{ $errors->first('email') }}

              {{ Form::label('password', 'Password: ') }}
              {{ Form::text('password', null, array('class'=>'form-control')) }}
              {{ $errors->first('password') }}
             
              {{ Form::label('name', 'Name: ') }}
              {{ Form::text('name', null, array('class'=>'form-control')) }}
              {{ $errors->first('name') }}
             
              {{ Form::label('image', 'Image: ') }}
              {{ Form::file('image') }}
              {{ $errors->first('image') }}
              <br/>
              {{ Form::radio('category', 'jobseeker')}}
              {{ Form::label('category', 'Job Seeker ') }}
              {{ $errors->first('category') }} 
              
              {{ Form::radio('category', 'employer')}}
              {{ Form::label('category', 'Employer ') }}
              {{ $errors->first('category') }} 
             <br/>
              {{ Form::label('phone', 'Phone: ') }}
              {{ Form::text('phone', null, array('class'=>'form-control')) }}
              {{ $errors->first('phone') }} 
              <br/>
              {{ Form::submit('Create', array('class'=>'btn btn-default')) }}

        {{ Form::close() }}

@overwrite