@extends('layout')
@section('title')
Edit Job
@stop
@section('column2')
        {{ Form::model($job, array('method'=>'PATCH', 'route'=> array('job.update', $job->id))) }}

        {{ Form::label('title', 'Job Title: ') }}  
        {{ Form::text('title', null, array('class'=>'form-control')) }}
 
        {{ Form::label('description', 'Description: ') }}
        {{ Form::text('description', null, array('class'=>'form-control')) }}
  
        {{ Form::label('location', 'Location: ') }}
        {{ Form::text('location', null, array('class'=>'form-control')) }} 

        {{ Form::label('salary', 'Salary: ') }}
        {{ Form::text('salary', null, array('class'=>'form-control')) }}
        <br/>
        {{ Form::submit('Update', array('class'=>'btn btn-default')) }}
        {{ Form::close() }}

@overwrite