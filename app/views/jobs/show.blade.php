@extends('layout')
@section('title')
Job Details
@stop
@section('column1')
        <div class="list-group">
            @if (Auth::check()) 
                <a href="{{route('job.index')}}" class="list-group-item">Back to Jobs</a>
            @endif
        </div>
@overwrite

@section('column2')
            <h1> Job Details</h1>

            <p> Employer Id: {{{$job->user_id}}}
            <p> Title: {{{$job->title}}} </p>
            <p> Description: {{{$job->description}}}</p>
            <p> Location: {{{$job->location}}}</p>
            <p> Salary: {{{$job->salary}}}</p>
            <p> Posted At: {{{$job->created_at}}}</p>
            <p> Last Updated: {{{$job->updated_at}}}</p>
            @if (Auth::user()->category == 'jobseeker')
            <p> {{link_to_route('application.create', 'Apply', $applcation = array($job->id)) }} </p>
            @else
            <p> {{link_to_route('job.edit', 'Edit', array($job->id)) }} </p>

            {{ Form::open(array('method' => 'DELETE', 'route'=>array('job.destroy', $job->id))) }}
            {{ Form::submit('Delete', array('class'=>'btn btn-danger')) }}
            {{Form::close()}}
@endif
@overwrite
