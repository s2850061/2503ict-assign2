@extends('layout')
@section('title')
Add Job
@stop 
@section('column2')
      <h1> Add Job</h1>
      {{ Form::open(array('action' =>   'JobController@store')) }}
            {{ Form::label('title', 'Job Title: ') }}
            {{ Form::text('title', null, array('class'=>'form-control')) }}
            {{ $errors->first('title') }}
  
            {{ Form::label('description', 'Description: ') }}
            {{ Form::text('description', null, array('class'=>'form-control')) }}
            {{ $errors->first('description') }}
  
            {{ Form::label('location', 'Location: ') }}
            {{ Form::text('location', null, array('class'=>'form-control')) }} 
            {{ $errors->first('location') }}
  
            {{ Form::label('salary', 'Salary: ') }}
            {{ Form::text('salary', null, array('class'=>'form-control')) }}
            {{ $errors->first('salary') }}
            <br/>
            {{ Form::submit('Create', array('class'=>'btn btn-default')) }}

      {{ Form::close() }}

@overwrite