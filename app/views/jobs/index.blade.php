@extends('layout')
@section('title')
Jobs
@stop

@section('column1')
@if (Auth::check()) 
    <div class="list-group">
      <img src="{{ Auth::user()->image->url('thumb') }}">
        @if (Auth::user()->category=='employer')
            <a href="job/create" class="list-group-item">Add Job</a>
            <a href="application/index" class="list-group-item">View Applications</a>
        @endif
    </div>
@endif
@overwrite

@section('column2')
    @if (Auth::check())
        <h1>Job List</h1>
            {{ Form::open(array('url'=>'job.search')) }}
              
            {{ Form::text('keyword', null, array('class'=>'form-control','placeholder'=>'search by keyword e.g title, location, salary, description')) }}
            <br/>
            {{Form::submit('search' , array('class'=>'btn btn-default'))}}
            {{Form::close() }}
        <br/>
        <ul>
            
            @foreach ($jobs as $job)
            @if ($job->id == Auth::user()->id)
                <li> {{link_to_route('job.show', $job->title, array($job->id)) }}</li>
           @endif
           @endforeach
           
          @foreach ($jobs as $job)
            @if (Auth::user()->category == 'jobseeker')
          <li> {{link_to_route('job.show', $job->title, array($job->id)) }}</li>
            @endif
          @endforeach
        </ul>

        {{ $jobs->links() }}

    @else
        <p>We help connect employers across Australia with the experienced and knowledgable staff that they need. With thousands of jobs being offered we're sure you'll find a career that suits you</p>
    @endif
@overwrite