@extends('layout')
@section('title')
Apply Summary
@stop
@section('column1')
        <div class="list-group">
            @if (Auth::check())
                <a href="{{route('job.index')}}" class="list-group-item">Back to Jobs</a>
            @endif
        </div>
@overwrite

@section('column2')
      <h1> Application </h1>
      <br/>
      Applicant Id: {{{$application->user_id}}}<br/>
      Job Id: {{{$application->job_id}}}<br/>
      Applicant Name: {{{$application->applicant}}} <br/>
      Application Letter: {{{$application->letter}}} <br/>
      Applied at: {{{$application->created_at}}}

@overwrite