@extends('layout')
@section('title')
Apply
@stop 
@section('column2')
      {{ Form::open(array('action' => 'ApplicationController@store')) }}
            {{ Form::label('applicant', 'Applicant name: ') }}
            {{ Form::text('applicant', null, array('class'=>'form-control')) }}
            {{ $errors->first('applicant') }}

            {{ Form::label('letter', 'Application letter: ') }}
            {{ Form::text('letter', null, array('class'=>'form-control')) }}
            {{ $errors->first('letter') }}
            <br/>
            {{ Form::submit('Apply', array('class'=>'btn btn-default')) }}
      {{ Form::close() }}

@overwrite