@extends('layout')
@section('title')
Applications
@stop
@section('content')

<ul>
  @foreach ($applications as $application)
  <li> {{link_to_route('application.show', $application->id, array($application->id)) }}</li>
  @endforeach
  
</ul>

{{ $application->links() }}


@overwrite