<?php 
class Application extends Eloquent
{ //Creates rules for appication form
  public static $rules = array(
  'letter' => 'required',
  'applicant' => 'required'
  );

 //Creates many to many relationship with users
 function user()
 {
 return $this->belongsToMany('User');
 }
  
 //Creates many to many relationship with jobs
 function job()
 {
 return $this->belongsToMany('Job');
 }

}

?>