<?php 
class Job extends Eloquent
{ //Creates rules for job creation form
  public static $rules = array(
  'title' => 'required',
  'description' => 'required',
  'location' => 'required',
  'salary' => 'required:min:0'
  );
  
 //creates one to many relationship to user table
 function user()
 {
 return $this->belongsTo('User');
 }

  //creates one to many relationship to user table
 function applications()
 {
 return $this->hasMany('Application');
 }
  
}

?>