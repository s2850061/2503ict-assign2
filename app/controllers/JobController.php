<?php

class JobController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  
  //shows index of jobs paginated by 4 and ordered by the creation date
	public function index()
	{
    $jobs = Job::orderBy('created_at', 'desc')->paginate(4);
    return View::make('jobs.index')->with('jobs',$jobs);
    
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  
  //Allows logged in employer users to add jobs
	public function create()
	{
    if (!Auth::check()) return Redirect::route('job.index');
		return View::make('jobs.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  
  //Allows logged in employer users to store added jobs
	public function store()
	{
    if (!Auth::check()) return Redirect::route('job.index');
		$input = Input::all();
    $v = Validator::make($input, Job::$rules);
    if ($v->passes())
    
    {
    $job = new Job();
    $job->user_id = Auth::user()->id;
    $job->title = $input['title'];
    $job->description = $input['description'];
    $job->location = $input['location'];
    $job->salary = $input['salary'];
    $job->updated_at;
    $job->save();
    return Redirect::route('job.show', $job->id);
    }
    
    else {
      return Redirect::route('job.create')->withErrors($v);
    }
    
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  
  //Shows job description for users 
	public function show($id)
	{ if (!Auth::check()) return Redirect::route('job.index');
   else {
		$job = Job::find($id);
    return View::make('jobs.show', compact('job'));
    }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  
  //Allows employer users to edit job description
	public function edit($id)
	{
    if (!Auth::check()) return Redirect::route('job.index');
		$job = Job::find($id);
    return View::make('jobs.edit')->with('job', $job);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  
  //Updates the job description
	public function update($id)
	{ 
    if (!Auth::check()) return Redirect::route('job.index');
    
		$job = Job::find($id);
    
    $input = Input::all();
    
    $job->title = $input['title'];
    $job->description = $input['description'];
    $job->location = $input['location'];
    $job->salary = $input['salary'];
    $job->save();
    return Redirect::route('job.show', $job->id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  
  //Destroys selected job if employer user
	public function destroy($id)
	{
    if (!Auth::check()) return Redirect::route('job.show');
		$job = Job::find($id);
    $job->delete();
    return Redirect::route('job.index');
	}
  
  //Allows job seekers to search via keywords *currently does not work
  public function search()
    {
    $keyword= Input::get('keyword');
    
    $jobs = Job::where('title', 'LIKE', '%'.$keyword.'%')->get();
    
    return Redirect::route('job.index');
    }
  
}

