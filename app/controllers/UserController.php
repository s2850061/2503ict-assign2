<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  
  //Links to the user index.blade.php
	public function index()
	{
    return View::make('user.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  
  //Links to the user create.blade.php
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  
  //Allows users to store entered inputs and create account
	public function store()
	{
    $input = Input::all();
    $encrypted = Hash::make($input['password']);
    $v = Validator::make($input, User::$rules);
    if ($v->passes())
    
    {
    $user = new User;
    $user->email= $input['email'];
    $user->password = $encrypted;
    $user->name = $input['name'];
    $user->category = $input['category'];
    $user->phone = $input['phone'];
    $user->image = $input['image'];
    $user->remember_token = "default";
    $user->save();
    return Redirect::route('job.index');
    }
      
    else {
      return Redirect::route('user.create')->withErrors($v);
    }
    
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    //
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
  
  //Allows users to log in and takes them to job index
  public function login()
  {
    $userdata=array(
    'email' => Input::get('email'),
    'password' => Input::get('password')
    );
    
    //attempt to do the login
    if (Auth::attempt($userdata)) {
      return Redirect::route('job.index');
    } else {
      return Redirect::to(URL::previous())->withInput();
    }
  }  
  
  //Allows users to log out and takes them to job index
  public function logout()
  {
    Auth::logout();
    return Redirect::action('JobController@index');
  }
}

