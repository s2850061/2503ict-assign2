<?php

class ApplicationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  
  //returns 2 paginated applications per pagination page
	public function index()
	{
		$applications = Application::paginate(2);
    return View::make('application.index', compact('applications'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  
  //creates link to application create.blade.php
	public function create()
	{
		return View::make('application.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  
  //Stores input data for application
	public function store()
	{
		$input = Input::all();
    $v = Validator::make($input, Application::$rules);
    if ($v->passes())
    
    {
    $application = new Application();
    $application->user_id = Auth::user()->id;
    $application->job_id = Job::find($job->id);
    $application->applicant = $input['applicant'];
    $application->letter = $input['letter'];
    $application->save();
    return Redirect::route('application.show', $application->id);
    }
    
    else {
      return Redirect::route('application.create')->withErrors($v);
    }
    
	}
  
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  
  //Creates link to show.blade.php in application folder and displays application details
	public function show($id)
	{
	  $application = Application::find($id);
    return View::make('application.show', compact('application'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}